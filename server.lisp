(in-package :ioa.re)

;; Server and initial dispatch table

(defvar *server*)

(setq hunchentoot:*dispatch-table*  ; dispatches folders, files, and pages
      (ioa.re.dispatchers:get-dispatch-table))

(defun start-website (&key (address "localhost") (port 8383))
  (setq *server* (hunchentoot:start
		  (make-instance 'easy-acceptor
				 :address address
				 :port port))))

(defun stop-website ()
  (stop *server*))

;;; debugging

(defun debug-mode-on ()
  (setq hunchentoot:*catch-errors-p* NIL)
  (setq hunchentoot:*show-lisp-errors-p* T))

(defun debug-mode-off ()
  (setq hunchentoot:*catch-errors-p* T)
  (setq hunchentoot:*show-lisp-errors-p* NIL))
