# ioa's website repository

This is the most current version of my website, which runs on a nixos server.

https://ioa.re

## ci/cd pipeline with ansible

After commiting changes to my local copy of this repository, my website gets updated with 
`./update-website` which pushes the changes and plays the ansible playbook `ansible/cicd.yml`.
