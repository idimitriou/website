;;;;posts.lisp

(in-package :ioa.re.posts)

;;; Manually creating and collecting post dispatchers.

(defun get-post-dispatchers ()
  (list
   (hunchentoot:create-prefix-dispatcher
    "/post/2019-09-13-hello-world-again"
    'hello-world-again-handler)))

;; hello-world

(defun hello-world-again-handler ()
  (with-output-to-string (stream)
    (html-template:fill-and-print-template
     #P"ioa.tmpl"
     (list :title "hello world, again"
	   :template '((#P"post/2019-09-13-hello-world-again.tmpl")))
     :stream stream)))
