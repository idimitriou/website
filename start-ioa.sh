#! /usr/bin/env nix-shell
#! nix-shell -i sh /home/ioa/quicklisp/local-projects/ioa.re/ioa.re.nix

# is the above nix-shell being replaced by 
# /run/current-system/sw/bin/nix-shell in the systemd-config
# invoked call? In a way, it must know its name.

exec ccl -b -l ~/quicklisp/local-projects/ioa.re/start-ioa.lisp

