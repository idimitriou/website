(defsystem "ioa.re"
  :description "ioa's hunchentoot website"
  :author "Ioanna M. Dimitriou H."
  :components ((:file "packages")
	       (:file "common"
		      :depends-on ("packages"))
	       (:file "posts"
		      :depends-on ("packages"))
	       (:file "dispatchers"
		      :depends-on ("packages"
				   "common"
				   "posts"))
	       (:file "server"
		      :depends-on ("packages"
				   "dispatchers")))
  :depends-on ("hunchentoot"
	       "html-template"))

