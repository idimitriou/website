(defpackage :ioa.re.common
  (:use :cl)
  (:documentation "Pathname and local directory functionality.")
  (:export :absolute-path-string))

(defpackage :ioa.re.posts
  (:use :cl)
  (:documentation "Dispatchers for each post.")
  (:export :get-post-dispatchers))

(defpackage :ioa.re.dispatchers
  (:use :cl
	:split-sequence
	:hunchentoot
	:html-template
	:ioa.re.common
	:ioa.re.posts)
  (:documentation "Collects all the dispatchers, and defines the index and posts page handlers.")
  (:export :get-dispatch-table))

(defpackage :ioa.re
  (:use :cl
	:hunchentoot
	:ioa.re.dispatchers)
  (:documentation "The Hunchentoot server setup for my personal website.")
  (:export :start-website
	   :stop-website))

