with import <nixpkgs> { };

stdenv.mkDerivation ({
      name = "ioa.re";
      buildInputs = [ ccl openssl ];
      LD_LIBRARY_PATH = lib.makeLibraryPath [ openssl ];
})
