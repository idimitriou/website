;;;;dispatchers.lisp

(in-package :ioa.re.dispatchers)

(defun get-dispatch-table ()
  (append
   (get-post-dispatchers)
   (list
    (hunchentoot:create-static-file-dispatcher-and-handler
     "/ioa.css" (absolute-path-string "www/ioa.css"))
    (hunchentoot:create-static-file-dispatcher-and-handler
     "/ioa.ico" (absolute-path-string "www/ioa.ico"))
    (hunchentoot:create-folder-dispatcher-and-handler
     "/pdfs/" (absolute-path-string "www/pdfs/"))
    (hunchentoot:create-folder-dispatcher-and-handler
     "/images/" (absolute-path-string "www/images/"))
    (hunchentoot:create-static-file-dispatcher-and-handler
     "/posts.xml" (absolute-path-string "www/posts.xml"))
    (hunchentoot:create-prefix-dispatcher
     "/posts" 'posts-handler)
    (hunchentoot:create-prefix-dispatcher
     "/" 'index-handler))))

;; setup html-template

(setq html-template:*default-template-pathname*
      (ioa.re.common:absolute-path-string "www/templates/"))

;;; Menu dispatchers

;; Index page

(defun index-handler ()
  (with-output-to-string (stream)
    (html-template:fill-and-print-template
     #P"ioa.tmpl"
     (list :title "ioa's website"
	   :template '((#P"index.tmpl")))
     :stream stream)))

;; posts page, rss feed, and individual posts

(defun posts-handler ()
  "The page listing the posts."
  (with-output-to-string (stream)
    (html-template:fill-and-print-template
     #P"ioa.tmpl"
     (list :title "ioa's posts"
	   :rss T
	   :template '((#P"posts.tmpl")))
     :stream stream)))
