;;;;common.lisp

(in-package :ioa.re.common)

;; Common utility functions related to file input and output.

(defvar *local-directory* "")
(setf *local-directory*
      (asdf:system-source-directory "ioa.re"))

(defun absolute-pathname (relative-path-string)
  (merge-pathnames relative-path-string *local-directory*))

(defun absolute-path-string (relative-path-string)
  (namestring (absolute-pathname relative-path-string)))
